/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sobel.android.creditcard;

import java.util.regex.Pattern;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sobel.android.hiddenbufferservice.HiddenContentHandle;
import com.sobel.android.secureinputservice.SecureInputManager;
import com.sobel.android.secureinputservice.SecureInputView;
import com.sobel.android.secureinputservice.SecureInputViewListener;
import com.sobel.jebpf.EBPFInstruction;
import com.sobel.jebpf.EBPFInstruction.InstructionCode;
import com.sobel.jebpf.EBPFInstruction.InstructionSize;
import com.sobel.jebpf.EBPFInstruction.Register;


/**
 * Credit Card entry demo
 */
public class CreditCardActivity extends Activity {
    /**
     * Called with the activity is first created.
     */

	private LinearLayout mRootLayout;
	private TextView mStatus1;
	private TextView mStatus2;

	private class InputListener implements SecureInputViewListener {

		private TextView mStatus;
		private SecureInputView mSiv;

		public InputListener(TextView status, SecureInputView siv) {
			mStatus = status;
			mSiv = siv;
		}
		
		@Override
		public void onEditStart() {
			mStatus.setText("editing");
		}

		@Override
		public void onEditStop() {
			HiddenContentHandle h = mSiv.getHiddenContentHandle();
			int length = h.getLength();
			boolean match = h.matchesPattern(Pattern.compile("\\d+"));
			int r = h.runEBPF(getEbpfCode());
			mStatus.setText(String.format("not editing, length: %d, match: %b, ebpf: %d", length, match, r));
			h.free();
		}
		
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set the layout for this activity.  You can find it
        // in res/layout/hello_activity.xml
        mRootLayout = (LinearLayout)getLayoutInflater().inflate(R.layout.creditcard_activity, null);
        
        mStatus1 = new TextView(this.getApplicationContext());
        mStatus1.setTextSize(TypedValue.COMPLEX_UNIT_SP, 8);
        mStatus1.setText("init");
        
        mStatus2 = new TextView(this.getApplicationContext());
        mStatus2.setTextSize(TypedValue.COMPLEX_UNIT_SP, 8);
        mStatus2.setText("init");
        
        SecureInputView siv1 = new SecureInputView(this.getApplicationContext());
        SecureInputView siv2 = new SecureInputView(this.getApplicationContext());
        siv1.setListener(new InputListener(mStatus1, siv1));
        siv2.setListener(new InputListener(mStatus2, siv2));

        mRootLayout.addView(mStatus1);
        mRootLayout.addView(siv1);

        mRootLayout.addView(mStatus2);
        mRootLayout.addView(siv2);
        
        setContentView(mRootLayout);
        EditText tv = (EditText)findViewById(R.id.text1);
        tv.setInputType(tv.getInputType() & (~InputType.TYPE_TEXT_FLAG_MULTI_LINE));
    }

    private byte[] getEbpfCode() {
    	// Return silly code that returns first byte.
    	EBPFInstruction[] code = new EBPFInstruction[] {
    			EBPFInstruction.LD_ABS(InstructionSize.W, 0),
    			EBPFInstruction.JMP_IMM(InstructionCode.JGT, Register.R0, 0, (short)2),
    				EBPFInstruction.MOV_IMM(Register.R0, -1),
    				EBPFInstruction.EXIT(),
    			EBPFInstruction.LD_ABS(InstructionSize.B, 4),
    			EBPFInstruction.EXIT(),
    	};
    	return EBPFInstruction.encodeMany(code);
    }
    
}

